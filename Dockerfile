FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y libsasl2-dev libldap2-dev libssl-dev python3-pip python3-dev && apt-get install -y locales&& \
    apt-get install -y libpq-dev && \
    pip3 install --upgrade pip setuptools


RUN locale-gen ru_RU.UTF-8

ENV PYTHONIOENCODING=UTF-8
ENV LANGUAGE ru_RU.UTF-8
ENV LANG ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8

WORKDIR /
COPY . /


RUN python3 setup.py develop

ENTRYPOINT [ "pserve" ]

CMD [ "production.ini" ]
