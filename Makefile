build:
	docker build --rm -t video_conf_prew .

run:
	docker run -d --name=video_conf_prew_con -p 9375:6543 -e VIRTUAL_HOST=video_conf_prew.nordsy.spb.ru -e VIRTUAL_PORT=9375 video_conf_prew:latest
	@echo "video_conf_prew_con running on http://localhost:9375"
	
delete:
	docker rm video_conf_prew_con
	@echo "video_conf_prew image deleted"
	
stop:
	docker stop video_conf_prew_con
	@echo "stop video_conf_prew_con"

logs:
	docker logs -f video_conf_prew_con
	
cprest:
	make stop
	make delete
	make build
	make run
