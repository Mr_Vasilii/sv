from pyramid.response import Response
from pyramid.view import view_config





@view_config(route_name='home', renderer='templates/MainPage.jinja2')
def home(request):
    return {
        "dir":"https://ya.ru",
        "sovpr":"https://mail.ru",
        "sroch":"http://www.nordsy.spb.ru/"
            }
